var cipherMap = {
    A: "Q",
    B: "W",
    C: "E",
    D: "R",
    E: "T",
    F: "Y",
    G: "U",
    H: "I",
    I: "O",
    J: "P",
    K: "A",
    L: "S",
    M: "D",
    N: "F",
    O: "G",
    P: "H",
    Q: "J",
    R: "K",
    S: "L",
    T: "Z",
    U: "X",
    V: "C",
    W: "V",
    X: "B",
    Y: "N",
    Z: "M"
}

var myCipherArray = []; //[W, Q, F, Q, F, Q]
var myCipher = ""; // WQFQFQ

var revCipherMap = {}; // COMPARING "WQFQFQ" TO KEYS TO PUSH VALUES INTO "deCipherArray"
var deCipherArray = []; //[B, A, N, A, N, A]
var deCiphered = ""; //BANANA

// Takes variable 'message' and creates the 'cipher code' from it using user input from a 'prompt();' within the function
function makeCipher() {
    var message = prompt(); //BANANA
    for (var a = 0; a < message.length; a++) {
        var b = message.charAt(a);
        if (cipherMap.hasOwnProperty(b)) {
            myCipherArray.push(cipherMap[b]);
        }
    }
    myCipher = (myCipherArray.join(""));
    console.log(myCipher);
}

// Swaps {keys: values} in 'cipherMap' for the decipher function, stored in a new object 'revCipherMap'
// User must type 'swap(cipherMap);' for this to function - is that obvious knowledge or should
// I have somehow made this function without having to input the 'cipherMap' variable?
function swap(cipherMap) {
    for (var c in cipherMap) {
        revCipherMap[cipherMap[c]] = c;
    }
    return revCipherMap;
}

// Takes variable 'myCipher' and deciphers it back into the original message 
function deCipher() {
    for (var d = 0; d < myCipher.length; d++) {
        var e = myCipher.charAt(d);
        if (revCipherMap.hasOwnProperty(e)) {
            deCipherArray.push(revCipherMap[e]);
        }
    }
    deCiphered = (deCipherArray.join(""));
    console.log(deCiphered);
}