const originalString = "I speak latin";
// Split string by whitespace character
const splitString = originalString.split(" ");

function makeLatin() {
    var num = 1;
    var makeLatinArray = [];
    for (var i = 0; i < splitString.length; i++) {
        makeLatinArray.push(
            splitString[i].substr(num) + "" + splitString[i].substr(0, num) + "ay"
        );
    }
    console.log(makeLatinArray.join(" "));
}

