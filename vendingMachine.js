function vendingMachine(snack, cash) {
    const SNACKS = [
        { name: 'Espresso', price: 1 },
        { name: 'Cappuccino', price: 2.50 },
        { name: 'Chocolate', price: 2 },
        { name: 'Potato', price: 3.50 }
    ];

    const SNACKSELECTED = SNACKS.find(item => item.name === snack);

    if (SNACKSELECTED) {
        if (SNACKSELECTED.price === cash) {
            //sorry, I'm an English teacher, the grammar must be correct!
            if (snack === "Potato Chips") {
                return `Your ${snack} have been served`;
            } else {
                return `Your ${snack} has been served`;;
            }
        }
        else {
            if (SNACKSELECTED.price > cash) {
                return `Insufficient funds. Please insert more cash.`;
            }
            else {
                return `Your ${snack} have been served. Here is your $${cash - SNACKSELECTED.price} change.`;
            }
        }
    }

    else {
        return `Do not try and buy a ${snack}, that's impossible. Instead, only try to realize the truth...
        there is no ${snack}. Then you will see it is not a ${snack} that you desire, it is only yourself.`
    }
};