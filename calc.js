// write a function called calc that takes 3 arguments the first 2 are numbers and the third is 
// an arithmetic operator, so it is either + , -, *, /
// and it executes the appropriate operation according to the provided arithmetic operator.
// make sure you test your function with all 4 arithmetic operations
// if doing + or - and num2 has no input, it should be 0
// if doing * or  and num2 has no input, it should be 0

var opArr = ["+", "-", "*", "/"];

function calc(a, b, c) {
    // ADDITION
    if (c === undefined && b === opArr[0]) {
        return a + 0;
    }
    else if (c === opArr[0]) {
        return a + b;
    }
    // SUBTRACTION
    else if (c === undefined && b === opArr[1]) {
        return a - 0;
    }
    else if (c === opArr[1]) {
        return a - b;
    }
    // MULTIPLICATION 
    else if (c === undefined && b === opArr[2]) {
        return a * 1;
    }
    else if (c === opArr[2]) {
        return a * b;
    }
    // DIVISION
    else if (c === undefined && b === opArr[3]) {
        return a / 1;
    }
    else if (c === opArr[3]) {
        return a / b;
    }
};