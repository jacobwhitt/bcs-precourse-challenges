// Write a function called swap that takes one argument, an object, 
// and returns another object where the key:value pairs have been swapped.
//The original object should not be modified.
// Example:

// var obj = { a: 1, b: 2 }
// var newObj = swap(obj)
// newObj // {1: 'a', 2: 'b'}

// read keys/values of an object obj[a] = obj[1]
// set keys to values and values to keys
// return new object with new keys:values

var obj = { A: 1, B: 2, C: 3, D: 4 };

function swap(obj) {
    var swapPed = {};
    for (var i in obj) {
        swapPed[obj[i]] = i;
    }
    return swapPed;
}