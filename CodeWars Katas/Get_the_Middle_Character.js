//? You are going to be given a word. Your job is to return the middle character of the word (as a string). If the word's length is odd, return the middle character. If the word's length is even, return the middle 2 characters.
//* Kata.getMiddle("test") should return "es"
//* Kata.getMiddle("testing") should return "t"
//* Kata.getMiddle("middle") should return "dd"
//* Kata.getMiddle("A") should return "A"
//! REMEMBER TO PUT STRINGS IN QUOTES WHEN CALLING THE FUNCTION!

//* MY SOLUTION
function getMiddle(string) {
    let position;
    let length;
    if (string.length % 2 === 0) {
        position = string.length / 2 - 1;
        length = 2;
    } else {
        position = ((string.length - 1) / 2);
        length = 1;
    }
    return string.substring(position, position + length);
}

//! BEST PRACTICES -> learn what ".ceil" does
function getMiddle(s) {
    return s.substr(Math.ceil(s.length / 2 - 1), s.length % 2 === 0 ? 2 : 1);
}