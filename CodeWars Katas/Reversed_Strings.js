//? Reverse a string

//* MY SOLUTION
function solution(str) {
    var splitString = str.split('');
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join('');
    return joinArray;
}

//! BEST PRACTICES / CLEVER - I knew it - you can stack methods!!
function solution(str) {
    return str.split('').reverse().join('');
}