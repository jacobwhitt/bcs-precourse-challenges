//? Return the correct letter grade based on the mean of 3 integers

//* MY SOLUTION
function getGrade(s1, s2, s3) {
    let mean = (s1 + s2 + s3) / 3;

    if (mean <= 100 && mean >= 90) {
        return 'A';
    } else if (mean < 90 && mean >= 80) {
        return 'B'
    } else if (mean < 80 && mean >= 70) {
        return 'C';
    } else if (mean < 70 && mean >= 60) {
        return 'D';
    } else {
        return 'F';
    }
}

//! BEST PRACTICES - simpler version of mine with less code; runs faster. learn to use "condition ? true : false" more for shorter, neatly-refactored code
function getGrade(s1, s2, s3) {
    var s = (s1 + s2 + s3) / 3
    return s >= 90 ? "A" : s >= 80 ? "B" : s >= 70 ? "C" : s >= 60 ? "D" : "F"
}