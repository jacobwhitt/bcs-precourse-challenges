//? You get an array of numbers, return the sum of all of the positives ones. Example[1, -4, 7, 12] => 1 + 7 + 12 = 20. Note: if there is nothing to sum, the sum is default to 0.

//* MY SOLUTION boo yah! figured out that there was a problem when a negative number appeared and it wasn't adding 'a' and instead skipped it completely (I think), but now it just adds 0 if there's a negative number!
function positiveSum(arr) {
    return arr.reduce(function (a, b) {
        return (a >= 0 && b >= 0) ? a + b : a + 0
    }, 0);
}

//! BEST PRACTICES
function positiveSum(arr) {
    return arr.reduce((a, b) => a + (b > 0 ? b : 0), 0);
}