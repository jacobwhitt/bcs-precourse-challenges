songDecoder("WUBWEWUBAREWUBWUBTHEWUBCHAMPIONSWUBMYWUBFRIENDWUB")
// =>  WE ARE THE CHAMPIONS MY FRIEND

//* MY SOLUTION - So it seems that I don't have to use variables and a RegEx at all and in fact many people didn 't and instead used simple methods using /(WUB)+/g and a trim() function I 'm unaware of. It seems that I need to go learn what putting the string within parentheses and using '+' means when using a "RegEx" search like /(text here)+/

function songDecoder(song) {
  let regex = /WUB|WUBWUB/gi;
  let stepOne = song.replace(regex, ' ')
  let originalSong = stepOne.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
  return originalSong;
}

//! BEST PRACTICES + CLEVER
function songDecoder(song) {
  return song.replace(/(WUB)+/g, " ").trim()
}