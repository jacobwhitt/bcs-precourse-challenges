//? Create a function that removes the first and last characters of a string.

//* MY SOLUTION
function removeChar(str) {
    return str.substring(1, str.length - 1)
};

//! BEST PRACTICES - learn to use 'slice()'
const removeChar = str => str.slice(1, -1)