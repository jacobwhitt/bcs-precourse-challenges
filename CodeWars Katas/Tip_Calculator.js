//? Complete the function, which calculates how much you need to tip based on the total amount of the bill and the service. You need to consider the following ratings:
// Terrible: tip 0 %
// Poor: tip 5 %
// Good: tip 10 %
// Great: tip 15 %
// Excellent: tip 20 %
//? The rating is case insensitive (so "great" = "GREAT"). If an unrecognised rating is received, then you need to return:
// "Rating not recognised" in Javascript, Python and Ruby...
// ...or null in Java
//...or - 1 in C#
//? Because you 're a nice person, you always round up the tip, regardless of the service.

//* MY SOLUTION is long and probably eats up way more memory than necessary. Should've used an object
function calculateTip(amount, rating) {
    rating = rating.toLowerCase();
    let tip;
    if (rating == "excellent") {
        tip = amount * 0.20;
    } else if (rating == "great") {
        tip = amount * 0.15;
    } else if (rating == "good") {
        tip = amount * 0.10;
    } else if (rating == "poor") {
        tip = amount * 0.05;
    } else if (rating == "terrible") {
        tip = amount - amount;
    } else {
        return "Rating not recognised"
    }
    let totalTip = Math.ceil(tip);
    return totalTip;
}

//! BEST PRACTICES - used an object to store the values
const TIPS = {
    "terrible": 0.0,
    "poor": 0.05,
    "good": 0.1,
    "great": 0.15,
    "excellent": 0.2
};

const calculateTip = (amount, rating) => {
    rating = rating.toLowerCase();
    return rating in TIPS ? Math.ceil(TIPS[rating] * amount) : "Rating not recognised";
};