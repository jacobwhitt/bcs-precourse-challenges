//? Create a function (or write a script in Shell) that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.

//* MY SOLUTION
function even_or_odd(number) {
    return (number % 2 === 0 ? 'Even' : 'Odd');
}

//! BEST PRACTICES - if something is 'true', I dont have to set the === operand ?
function even_or_odd(number) {
    return number % 2 ? "Odd" : "Even"
}