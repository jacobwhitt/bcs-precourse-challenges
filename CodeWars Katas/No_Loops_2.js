//? You will be given an array (a) and a value (x). All you need to do is check whether the provided array contains the value, without using a loop.
//? Array can contain numbers or strings. X can be either. Return true if the array contains the value, false if not. With strings you will need to account for case.


//* MY SOLUTION - this sometimes failed the random values but because I cant SEE them...I have no idea whether they were strings or numbers. Useless...
check = (a, x) => a.includes(x) ? true : a.includes(x.toString.toLowerCase) ? true : a.includes(/x/i) ? true : false

//! BEST PRACTICES - I really do wanna smash my face into the keyboard when I realize how stupidly simple this answer was...
function check(a, b) {
    return a.includes(b);
};