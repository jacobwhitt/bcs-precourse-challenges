//? Return the sum of the contents of two arrays as a number

//* MY SOLUTION
function arrayPlusArray(arr1, arr2) {
    let reducer = (accumulator, currentValue) => accumulator + currentValue;
    return arr1.reduce(reducer) + arr2.reduce(reducer);
}

//! BEST PRACTICE CODE - the person apparently didn't set the current value to 0
//! so as to avoid throwing an error by reducing an empty array, so people 
//! suggested to set the second reduce argument to 0 to have a default value.
function arrayPlusArray(arr1, arr2) {
    return arr1.concat(arr2).reduce((acc, cur = 0) => acc + cur);
}