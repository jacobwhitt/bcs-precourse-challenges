//? Implement a function named generateRange(min, max, step), which takes three arguments and generates a range of integers from min to max, with the step. The first integer is the minimum value, the second is the maximum of the range and the third is the step. (min < max) NOTE: min < max. step > 0. range does not HAVE to include max (depending on step)


//* MY SOLUTION
function generateRange(min, max, step) {
    var arr1 = [min, ];
    for (i = min; i < max;) {
        var num = i + step;
        if (num > max) {
            return arr1;
        } else {
            arr1.push(num);
            i = i + step;
        }
    }
    return arr1;
}


//! BEST PRACTICE - I KNEW I should have been able to minimize this without using if/else, but I didn't know I could set the arguments in the for loop so simply like this solution did. Not sure what the "+=" is doing either.
function generateRange(min, max, step) {
    let arr = [];
    for (let i = min; i <= max; i += step) {
        arr.push(i);
    }
    return arr;
}