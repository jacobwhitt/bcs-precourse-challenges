//? Get the number n(n > 0) to return the reversed sequence from n to 1. Example: n = 5 >> [5, 4, 3, 2, 1]

//* MY SOLUTION
const reverseSeq = n => {
    arr = [];
    let arrRev = arr.reverse();
    arr.push(n)
    while ((n - 1) > 0) {
        arr.push(n - 1);
        n--;
    }
    return arrRev;
};


//! BEST PRACTICES
const reverseSeq = n => {
    let arr = [];
    for (let i = n; i > 0; i--) {
        arr.push(i);
    }
    return arr;
};