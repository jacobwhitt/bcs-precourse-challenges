//? Make an integer its opposite

//* MY SOLUTION
function opposite(num) {
    return num * -1
}

//! BEST PRACTICES
const opposite = number => -number;