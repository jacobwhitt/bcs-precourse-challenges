//? Americans are odd people: in their buildings, the first floor is actually the ground floor and there is no 13th floor ('cause of superstition). Write a function that given an American floor(passed as an integer) returns the real floor. Moreover, your function should work for basement floors too: just return the same value as the passed one.

//* MY SOLUTION IS THE BEST PRACTICE! WOO!
getRealFloor = (n) => n > 0 ? (n - 1) : n == 14 ? (n - 1) : n

//! BEST PRACTICES
getRealFloor = (n) => n > 13 ? (n - 2) : n > 0 ? (n - 1) : n